//
//  ViewController.swift
//  FirebaseStorageSample
//
//  Created by Oka Yuya on 2016/09/19.
//  Copyright © 2016年 nnsnodnb.moe. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let ud = UserDefaults.standard
        ud.set(0, forKey: "count")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func selectImageWithLibrary(_ sender: AnyObject) {
        pickImageFromLibrary()
    }
    
    func countPhoto() -> String {
        let ud = UserDefaults.standard
        let count = ud.object(forKey: "count") as! Int
        ud.set(count + 1, forKey: "count")
        return String(count)
    }
}


// MARK: UINavigationControllerDelegate
extension ViewController: UINavigationControllerDelegate {
    func pickImageFromLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            let controller = UIImagePickerController()
            controller.delegate = self
            controller.sourceType = UIImagePickerControllerSourceType.photoLibrary
            
            present(controller, animated: true, completion: nil)
        }
    }
}

// MARK: UIImagePickerControllerDelegate
extension ViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo
        info: [String : Any]) {
        let storage = FIRStorage.storage()
        let storageRef = storage.reference(forURL: "gs://<YOUR-GS-APP>.appspot.com")
        
        if let data = UIImagePNGRepresentation(info[UIImagePickerControllerOriginalImage] as! UIImage) {
            let reference = storageRef.child("image/" + NSUUID().uuidString + "/" + countPhoto() + ".jpg")
            reference.put(data, metadata: nil, completion: { metaData, error in
                print(metaData)
                print(error)
            })
            dismiss(animated: true, completion: nil)
        }
    }
}
